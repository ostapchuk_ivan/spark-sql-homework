package ostapchuk

import java.io.File

import org.apache.commons.io.FileUtils
import org.apache.spark.sql.SparkSession

object SparkDataFrame {
    def main(args: Array[String]): Unit = {
        if (args.length != 3) {
            println("Usage: <master> <AwardsPlayers.csv Path> <Master.csv Path>")
            return
        }

        val resultsPath = "results.csv"

        FileUtils.deleteQuietly(new File(resultsPath))

        val spark = SparkSession
            .builder()
            .appName("SparkSQL")
            .master(args(0))
            .getOrCreate()

        import spark.implicits._

        val awardsPlayersDF = spark.read.format("csv")
            .option("sep", ",")
            .option("inferSchema", "true")
            .option("header", "true")
            .load(args(1))
            .createTempView("awardsPlayers")

        val masterDF = spark.read.format("csv")
            .option("sep", ",")
            .option("inferSchema", "true")
            .option("header", "true")
            .load(args(2))
            .createTempView("master")

        val playerIDAndAward = spark.sql("SELECT playerID, award FROM awardsPlayers")

        val IDAndFullName = spark.sql("SELECT playerID, firstName, lastName FROM master")

        val IDAndNumber = playerIDAndAward
            .groupBy("playerID").count()

        val joined = IDAndNumber.join(IDAndFullName, "playerID")
            .select("firstName", "lastName", "count")
            .map(s => s.getAs[String](0) + " " + s.getAs[String](1) + " - " + s.getAs[String](2))

        joined.foreach(s => println(s))

        joined.coalesce(1).write
            .format("com.databricks.spark.csv")
            .save(resultsPath)

        spark.stop()
    }
}
